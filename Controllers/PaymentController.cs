using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PaymentController : ControllerBase
    {
        private readonly PaymentContext _context;

        public PaymentController(PaymentContext context)
        {
            _context = context;
        }
        
        //-----------------------------------------------------------------------------------
        // Inclusão da Venda  -- ok
        [HttpPost("Venda/{id}")]
        public IActionResult InclusaoVenda(Venda venda)
        {
            venda.StatusVenda = EnumStatusVenda.Aguardando_Pagamento;

            _context.Add(venda);
            _context.SaveChanges();

            return CreatedAtAction(nameof(PesquisaVendaId), new { Id = venda.Id }, venda);
        }

        // Consulta por ID da Venda -- ok
        [HttpGet("Venda/{id}")]
        public IActionResult PesquisaVendaId(int id)
        {
            var vendasBanco = _context.Vendas.Find(id);

            if (vendasBanco == null)
                return NotFound();
                
            return Ok(vendasBanco);
        }

        // Altera Status Venda  -- ok
        [HttpPut("Venda/StatusVenda/{id}/{statusVenda}")]
        public IActionResult AlteraStatusVenda(int id, EnumStatusVenda statusVenda, Venda venda)
        {
            /*
                0 - Aguardando_Pagamento,
                1 - Pagamento_Aprovado,
                2 - Enviado_Transportadora,
                3 - Entregue,	
                4 - Cancelada,
            */ 
            var pesquisaVendaId = _context.Vendas.Find(id);

            if(pesquisaVendaId == null)
                return NotFound();

            // Regra 1 - Validação lista Status Venda Possíveis
            if(statusVenda != EnumStatusVenda.Pagamento_Aprovado ||
               statusVenda != EnumStatusVenda.Enviado_Transportadora ||
               statusVenda != EnumStatusVenda.Entregue ||
               statusVenda != EnumStatusVenda.Cancelada) 
               return NoContent();               

            // Regra 2 - Validação Status Venda - Aguardando Pagamento
            if(pesquisaVendaId.StatusVenda == EnumStatusVenda.Aguardando_Pagamento) {
                if (statusVenda != EnumStatusVenda.Pagamento_Aprovado &&
                    statusVenda != EnumStatusVenda.Cancelada)
                return NoContent();
            }

            // Regra 3 - Validação Status Venda - Pagamento Aprovado
            if(pesquisaVendaId.StatusVenda == EnumStatusVenda.Pagamento_Aprovado) {
                if (statusVenda != EnumStatusVenda.Enviado_Transportadora &&
                    statusVenda != EnumStatusVenda.Cancelada)
                return NoContent();
            }  

            // Regra 4 - Validação Status Venda - Enviado para Transportadora
            // Regra 4 - Validação Status Venda - Enviado para Transportadora
            if(pesquisaVendaId.StatusVenda == EnumStatusVenda.Enviado_Transportadora) {
                if(statusVenda != EnumStatusVenda.Entregue)
                return NoContent();
            }

            pesquisaVendaId.StatusVenda = statusVenda;

            _context.Vendas.Update(pesquisaVendaId);
            _context.SaveChanges();

            return Ok();
        }


        //-----------------------------------------------------------------------------------
        // Inclusão de Mercadoria  -- ok
        [HttpPost("Mercadoria/{id}")]
        public IActionResult InclusaoMercadoria(Mercadoria mercadoria)
        {
            _context.Add(mercadoria);
            _context.SaveChanges();

            return CreatedAtAction(nameof(PesquisaMercadoriaId), new { Id = mercadoria.Id }, mercadoria);
        }

        // Consulta de Mercadoria por Id -- ok
        [HttpGet("Mercadoria/{id}")]
        public IActionResult PesquisaMercadoriaId(int id)
        {
            var mercadoriaBanco = _context.Mercadorias.Find(id);

            if (mercadoriaBanco == null)
                return NotFound();
                
            return Ok(mercadoriaBanco);
        }

        // Alteração de Mercadoria  -- ok
        [HttpPut("Mercadoria/{id}")]
        public IActionResult AlteraMercadoria(int id, Mercadoria mercadoria)
        {
            var mercadoriaBanco = _context.Mercadorias.Find(id);

            if(mercadoriaBanco == null)
                return NotFound();

            mercadoriaBanco.Nome = mercadoria.Nome;
            mercadoriaBanco.Quantidade = mercadoria.Quantidade;

            _context.Mercadorias.Update(mercadoriaBanco);
            _context.SaveChanges();

            return Ok();
        }

        // Exclusão de Mercadoria -- ok
        [HttpDelete("Mercadoria/{id}")]
        public IActionResult ExcluiMercadoria(int id)
        {
            var mercadoriaBanco = _context.Mercadorias.Find(id);

            if (mercadoriaBanco == null)
                return NotFound();

            _context.Mercadorias.Remove(mercadoriaBanco);
            _context.SaveChanges();

            return NoContent();
        }


        //-----------------------------------------------------------------------------------        
        // Inclusão de Vendedor  -- ok
        [HttpPost("Vendedor/{id}")]
        public IActionResult InclusaoVendedor(Vendedor vendedor)
        {
            _context.Add(vendedor);
            _context.SaveChanges();

            return CreatedAtAction(nameof(PesquisaVendedorId), new { Id = vendedor.Id }, vendedor);
        }

        // Consulta por ID do Vendedor  -- ok
        [HttpGet("Vendedor/{id}")]
        public IActionResult PesquisaVendedorId(int id)
        {
            var vendedorBanco = _context.Vendedores.Find(id);

            if (vendedorBanco == null)
                return NotFound();
                
            return Ok(vendedorBanco);
        }

        // Alteração de Vendedor
        [HttpPut("Vendedor/{id}")]
        public IActionResult AlteraVendedor(int id, Vendedor vendedor)
        {
            var vendedorBanco = _context.Vendedores.Find(id);

            if(vendedorBanco == null)
                return NotFound();

            vendedorBanco.Cpf = vendedor.Cpf;
            vendedorBanco.Nome = vendedor.Nome;
            vendedorBanco.Email = vendedor.Email;
            vendedorBanco.Telefone = vendedor.Telefone;

            _context.Vendedores.Update(vendedorBanco);
            _context.SaveChanges();

            return Ok();
        }

        // Exclusão de Vendedor -- ok
        [HttpDelete("Vendedor/{id}")]
        public IActionResult ExcluiVendedor(int id)
        {
            var vendedorBanco = _context.Vendedores.Find(id);

            if (vendedorBanco == null)
                return NotFound();

            _context.Vendedores.Remove(vendedorBanco);
            _context.SaveChanges();

            return NoContent();
        }
    }

    //-----------------------------------------------------------------------------------
    // 
}