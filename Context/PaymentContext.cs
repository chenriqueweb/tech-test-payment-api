using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Context
{
    public class PaymentContext : DbContext
    {
        public PaymentContext(DbContextOptions<PaymentContext> options) : base(options)
        {
        }

        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<ItensVenda> ItensVendas { get; set; }
        public DbSet<Mercadoria> Mercadorias { get; set; }

        //public DbSet<Pedido> Pedidos { get; set; }
    }
}
