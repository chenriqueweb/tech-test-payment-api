/* Massa de Dados */

----------------------------------------------------------------------------------------------
/* Mercadorias */
----------------------------------------------------------------------------------------------
SELECT TOP (1000) [Id]
      ,[Nome]
      ,[Quantidade]
  FROM [Payment].[dbo].[Mercadorias];

DELETE FROM [Payment].[dbo].[Mercadorias];

INSERT INTO [Payment].[dbo].[Mercadorias]
           ([Nome], [Quantidade])
    VALUES ('Mercadoria-1', 11);

INSERT INTO [Payment].[dbo].[Mercadorias]
           ([Nome], [Quantidade])
    VALUES ('Mercadoria-2', 22);
	
INSERT INTO [Payment].[dbo].[Mercadorias]
           ([Nome], [Quantidade])
    VALUES ('Mercadoria-3', 33);


----------------------------------------------------------------------------------------------
/* Vendedores */
----------------------------------------------------------------------------------------------
SELECT TOP (1000) [Id]
      ,[Cpf]
      ,[Nome]
      ,[Email]
      ,[Telefone]
  FROM [Payment].[dbo].[Vendedores];

DELETE FROM [Payment].[dbo].[Vendedores];

INSERT INTO [Payment].[dbo].[Vendedores];
           ([Cpf], [Nome], [Email], [Telefone])
    VALUES ('111.111.111-11', 'Vendedor-1', 'vendedor1@payment.com.br', '(11) 91111-1111');

INSERT INTO [Payment].[dbo].[Vendedores];
           ([Cpf], [Nome], [Email], [Telefone])
    VALUES ('222.222.222-22', 'Vendedor-2', 'vendedor2@payment.com.br', '(11) 92222-2222');

INSERT INTO [Payment].[dbo].[Vendedores];
           ([Cpf], [Nome], [Email], [Telefone])
    VALUES ('333.333.333-33', 'Vendedor-3', 'vendedor3@payment.com.br', '(11) 93333-333');


----------------------------------------------------------------------------------------------
/* Vendas */
----------------------------------------------------------------------------------------------
SELECT TOP (1000) [Id]
      ,[IdVendedor]
      ,[DataVenda]
      ,[StatusVenda]
      ,[Pedido]
  FROM [Payment].[dbo].[Vendas];

DELETE FROM [Payment].[dbo].[Vendas];

UPDATE  [Payment].[dbo].[Vendas]
SET [StatusVenda] = 1
WHERE ID = 1;

UPDATE  [Payment].[dbo].[Vendas]
SET [StatusVenda] = 0
WHERE ID = 2;

UPDATE  [Payment].[dbo].[Vendas]
SET [StatusVenda] = 2
WHERE ID = 3;

INSERT INTO [Payment].[dbo].[Vendas]         
            ([IdVendedor], [DataVenda], [StatusVenda], [Pedido])
     VALUES (1, CURRENT_TIMESTAMP, 1, 111);

INSERT INTO [Payment].[dbo].[Vendas]         
            ([IdVendedor], [DataVenda], [StatusVenda], [Pedido])
     VALUES (1, CURRENT_TIMESTAMP, 0, 222);

INSERT INTO [Payment].[dbo].[Vendas]         
            ([IdVendedor], [DataVenda], [StatusVenda], [Pedido])
     VALUES (1, CURRENT_TIMESTAMP, 2, 333);


----------------------------------------------------------------------------------------------
/* Itens de Venda */
----------------------------------------------------------------------------------------------
SELECT TOP (1000) [Id]
      ,[IdVenda]
      ,[Sequencia]
      ,[Pedido]
      ,[IdMercadoria]
      ,[Quantidade]
  FROM [Payment].[dbo].[ItensVendas];

DELETE FROM [Payment].[dbo].[ItensVendas];

INSERT INTO [Payment].[dbo].[ItensVendas]
           ([IdVenda], [Sequencia], [Pedido], [IdMercadoria], [Quantidade])
    VALUES (1, 1, 1, 1, 2);

INSERT INTO [Payment].[dbo].[ItensVendas]
           ([IdVenda], [Sequencia], [Pedido], [IdMercadoria], [Quantidade])
    VALUES (1, 2, 1, 1, 3);

