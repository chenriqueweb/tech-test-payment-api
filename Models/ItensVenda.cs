using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class ItensVenda
    {
        public int Id { get; set; }

        public int IdVenda  { get; set; }
        public int Sequencia { get; set; }
        
        public int Pedido { get; set; }
        public int IdMercadoria { get; set; }
        public int Quantidade { get; set; } 

        private Venda venda;

        // Construtores
        public ItensVenda() {}

        public ItensVenda(int id, int idVenda, int sequencia, int pedido, 
                          int idMercadoria, int quantidade, Venda venda)
        {
            Id = id;
            IdVenda = idVenda;
            Sequencia = sequencia;
            Pedido = pedido;
            IdMercadoria = idMercadoria;
            Quantidade = quantidade;
        }
    }
}