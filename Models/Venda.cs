namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public int IdVendedor { get; set; }
        public DateTime DataVenda { get; set; }
        public EnumStatusVenda StatusVenda { get; set; }
        public int Pedido { get; set; }

        public List<ItensVenda> Itens = new ArrayList<ItensVenda>();

        // Construtores
        public Venda() {}

        public Venda(int id, int idVendedor, DateTime dataVenda, EnumStatusVenda statusVenda,
                     int pedido, List<ItensVenda> itens)
        {
            Id = id;
            IdVendedor = idVendedor;
            DataVenda = dataVenda;
            StatusVenda = statusVenda;
            Pedido = pedido;
            Itens = itens;
        }

        public class ArrayList<T> : List<ItensVenda>
        {
        }
    }
}